addSbtPlugin("com.typesafe" % "sbt-mima-plugin" % "1.0.0")  // compatibility testing
addSbtPlugin("org.scala-js" % "sbt-scalajs"     % "1.7.0")  // cross-compile for scala.js
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.1.0")

